FROM python:3.7.13-alpine3.16
COPY flasktodo/ app/
RUN pip install -r app/requirements.txt
WORKDIR app
CMD ["flask","run","--host=0.0.0.0"] 
