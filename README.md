##          TODO Python App          ##


<center><h3> the app architecture in kubernetes cluster </h3> </center><br />
![architecture](architecture.jpg)



## designed to run on the cloud

this project is contains a set of tools that are integrated with each other - to have an high availability Production environment.

*) Git - source code management <br />
*) Bash - End To End Testing (CI) <br />
*) Docker - dockerize the todo app for CI process and for Production . <br />
*) Jenkins - CI process. <br />
*) terraform - provision an EKS cluster on AWS. <br />
*) Kubernetes - managing containerized workloads and services <br />
   -) Helm - install (ToDo app , EFK , Ingress-nginx , Grafana-Prometheus) Charts <br />


## to run locally
to provision the app , it must have a DB . <br />
*) it possible to run the app with mongodb online DB service. <br />
it`s prefered to use the docker-compose file , there are 2 docker-compose files : <br />
  A) docker-compose-no-nginx.yaml : has no nginx container <br />
  B) docker-compose.yaml : has a nginx container <br />
  
 command :  docker-compose -f "docker-compose-file-name" up <br />
 can access via : localhost:80 
 
 
