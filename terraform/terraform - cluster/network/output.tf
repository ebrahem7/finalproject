#export private subnets ids 
output "subnets_private_ids" {
    value = tolist("${aws_subnet.private_subnets.*.id}")
}