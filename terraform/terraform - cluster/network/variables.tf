variable "vpc_id" {
  description = "vpc id"
  
}

variable "cidr_block_vpc" {
  description = "cidr block of vpc"
  
}

variable "azs_public" {
  type= list
  default = ["us-east-2a","us-east-2b"]
}

variable "public_ip_on_launch" {
  default = false
  
}


## private subnet variables 
variable "azs_private" {
  type= list
  default = ["us-east-2a","us-east-2b"]
}
