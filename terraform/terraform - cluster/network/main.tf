resource "aws_subnet" "public_subnets" {

  count                   = "${length(tolist(var.azs_public))}"
  vpc_id                  = var.vpc_id
  cidr_block              = "${cidrsubnet(var.cidr_block_vpc,8,count.index)}"
  map_public_ip_on_launch = var.public_ip_on_launch
  availability_zone       = var.azs_public[count.index]
  tags = {
    "Name" = "Ebrahem_portfolio_subent_public_${count.index}"
  }

}


resource "aws_subnet" "private_subnets" {
  count                   = "${length(tolist(var.azs_private))}"
  vpc_id                  = var.vpc_id
  cidr_block              = "${cidrsubnet(var.cidr_block_vpc,8,count.index+10)}"
  availability_zone       = var.azs_private[count.index]
  tags = {
    "Name" = "Ebrahem_portfolio_subent_private_${count.index}"
  }

}


resource "aws_internet_gateway" "gw" {
  vpc_id = var.vpc_id

  tags = {
    "Name" = "Ebrahem_igw"
  }
}

resource "aws_route_table" "terraform_RT" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    "Name" = "Ebrahem_portfolio_RT"
  }
 }

resource "aws_route_table_association" "rt_association_a" {
  count          = "${length(var.azs_public)}"
  subnet_id      = "${element(aws_subnet.public_subnets.*.id,count.index)}"
  route_table_id = aws_route_table.terraform_RT.id

}


resource "aws_eip" "eip_address" {
  count = "${length(var.azs_public)}"
  vpc = true
}

resource "aws_nat_gateway" "nat_gw" {

  count          = "${length(var.azs_public)}"
  subnet_id      = "${element(aws_subnet.public_subnets.*.id,count.index)}"
  allocation_id = "${element(aws_eip.eip_address.*.id,count.index)}"

  tags = {
    Name = "gw NAT"
  }
}


resource "aws_route_table" "rt_NAT" {
    count          = "${length(tolist(var.azs_private))}"
    vpc_id = var.vpc_id
    
route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${element(aws_nat_gateway.nat_gw.*.id,count.index)}"
    }
tags = {
        Name = "Main Route Table for Private subnet"
    }
}

resource "aws_route_table_association" "rt_associate_private" {
  count          = "${length(var.azs_private)}"
  subnet_id      = "${element(aws_subnet.private_subnets.*.id,count.index)}"
  route_table_id = "${element(aws_route_table.rt_NAT.*.id,count.index)}"
}