variable "cidr_block" {
  default = "10.0.0.0/16"
}

variable "region_vpc" {
    default  = "us-east-2"
}
