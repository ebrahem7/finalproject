provider "aws" {
  region = var.region_vpc
}

resource "aws_vpc" "Ebrahem-portfolio" {
  cidr_block = var.cidr_block

  tags = {
    "Name" = "Ebrahem-portfolio"
  }
}


terraform {
  backend "s3" {
    bucket = "ebrahem-tfstate"
    key    = "ebrahem-tfstate/terraform.tfstate"
    region = "us-east-2"

  }
}

module "compute" {

  source    = "./compute"
  vpc_id    = aws_vpc.Ebrahem-portfolio.id
  subnets_private_ids = module.network.subnets_private_ids
}

module "network" {

  source              = "./network"
  vpc_id              = aws_vpc.Ebrahem-portfolio.id
  cidr_block_vpc      = var.cidr_block
}