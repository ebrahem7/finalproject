variable "vpc_id" {
  description = "vpc id"
  
}

variable "subnets_private_ids" {
  description = "get subnets ids from network module"
}


variable "instance-type" {
    default = "t2.large"
}

variable "desired-nodes" {
    default = 2
}

variable "max-nodes" {
    default = 2
}

variable "min-nodes" {
    default = 2
}

