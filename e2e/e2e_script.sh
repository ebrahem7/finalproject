#!/bin/bash

## get main page test
curl -s http://164.92.179.185:80 | grep 'WHAT DO YOU WANT TO DO TODAY?'
if [ `echo $?` != "0" ]; then
exit 1
fi


# ## post(add) new value
curl -d new-todo=jenkins-test-value -X POST http://164.92.179.185:80/add
curl -s http://164.92.179.185:80 | grep 'jenkins-test-value'
if [ `echo $?` != "0" ]; then
exit 1
fi

# ## delete all values 
curl -X GET http://164.92.179.185:80/delete_all | grep "list-group-item"
if [ `echo $?` != "1" ]; then
exit 1
fi
